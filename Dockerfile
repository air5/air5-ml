FROM tensorflow/tensorflow:2.1.1

WORKDIR /project

ADD . /project

RUN pip install -r requirements.txt

CMD ["python", "index.py"]