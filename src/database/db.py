from flask_pymongo import PyMongo

mongo = PyMongo()

def init_db(app):
    app.config['MONGO_DBNAME'] = 'air5'
    app.config['MONGO_URI'] = 'mongodb://air5:Air5Pass@cluster0-shard-00-00-vgmlk.gcp.mongodb.net:27017,cluster0-shard-00-01-vgmlk.gcp.mongodb.net:27017,cluster0-shard-00-02-vgmlk.gcp.mongodb.net:27017/air5?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority'
    mongo.init_app(app)
    
def standardized_arr(elements):
    return [standardized_obj(e) for e in elements]

def standardized_obj(element):
    element['id'] = str(element['_id'])
    element.pop('_id', None)
    element.pop('_class', None)
    
    return element

def Products():
    return mongo.db.product

def UserPoints():
    return mongo.db.userPoint

def RecommendProducts():
    return mongo.db.recommendProduct