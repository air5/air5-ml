import os
import pandas as pd
import numpy as np
import tensorflow

from numpy import dot
from numpy.linalg import norm
from tensorflow.keras.models import model_from_json
from ..database.db import RecommendProducts, UserPoints, standardized_obj
from scipy.spatial.distance import cdist

path = os.path.dirname(__file__) + '/'
# Load data
data = pd.read_csv(path + 'data.csv')
items_cate = data['name'].astype("category").cat.categories
items_code = data['name'].astype("category").cat.codes
items_length = len(items_cate.values)
print("Data loaded!!!")

# Matrix
combined = data.drop_duplicates(['user', 'name'])
combined['point'] = 1
matrix = combined.pivot(index='user', columns='name', values='point')
matrix.fillna(0, inplace=True)
item_mapping = {matrix.columns[i]: i for i in range(len(matrix.columns))}
print("Matrix loaded!!!")

# Load model
json_file = open(path + 'model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights(path + "model.h5")
print("Model loaded!!!")


def predict(user_id, N = 20):
    uid = get_similarity_user(user_id)
    i_test = np.unique(items_code.values)
    u_test = [uid for i in range(items_length)]
    
    i_test = np.array(i_test).reshape(-1, 1)
    u_test = np.array(u_test).reshape(-1, 1)

    out = loaded_model.predict([u_test, i_test])
    out = [o[0] for o in out]

    res = sorted(range(len(out)), key=lambda sub: out[sub])[-N:]

    idx = i_test[res]
    items = items_cate[idx][:N][::-1]
    items = [i[0] for i in items]

    recommend_product = RecommendProducts().find_one({'userId': user_id})

    if not recommend_product:
        recommend_product = {
            'userId': user_id,
            'productNames': []
        }

    recommend_product['productNames'] = items
    RecommendProducts().save(recommend_product)

    return standardized_obj(recommend_product)


def cos_sin(a, b):
    return dot(a, b)/(norm(a)*norm(b))


def get_similarity_user(user_id):
    arr = [0 for i in range(items_length)]
    user_point = UserPoints().find_one({'userId': user_id})

    if user_point:
        product_points = user_point['productPoints']
        ids = sorted(range(len(product_points)), key=lambda x: product_points[x]['point'])[-15:]
        product_points = [product_points[i] for i in range(len(product_points)) if i in ids]

        for product_point in product_points[-5:]:
            if product_point['productName'] in item_mapping:
                arr[item_mapping[product_point['productName']]] = 1

        user_point['productPoints'] = product_points
        user_point['time'] = 0

        UserPoints().save(user_point)

    result = 1 - cdist([arr], matrix.values, 'cosine')

    return np.argmax(result)
