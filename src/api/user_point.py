from flask import Blueprint, request
from ..database.db import standardized_obj, UserPoints
from ..processing.model import predict

user_point = Blueprint('user_point', __name__)


@user_point.route('/<user_id>', methods=['PUT'])
def update_user_point(user_id):
    product_names = sorted(request.json['productNames'])
    point = request.json['point']
    user_point = UserPoints().find_one({'userId': user_id})

    if not user_point:
        user_point = {
            'userId': user_id,
            'productPoints': [],
            'key': '',
            'time': 0
        }

    new_key = "".join(product_names)

    if user_point['key'] != new_key:
        user_point['key'] = new_key
        user_point['time'] += 1

        product_points = user_point['productPoints']
        old_product_names = [p['productName'] for p in product_points]

        product_points.extend(
            {'productName': n, 'point': 0} for n in product_names if n not in old_product_names)

        ids = [i for i in range(len(product_points))
            if product_points[i]['productName'] in product_names]
        
        for i in range(len(product_points)):
            if i in ids:
                product_points[i]['point'] += point
            else:
                product_points[i]['point'] *= 0.7

        UserPoints().save(user_point)

    return predict(user_id)