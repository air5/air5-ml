from flask import Flask
from flask import jsonify
from flask import request
from src.database.db import init_db, Products, standardized_arr
from src.api.user_point import user_point

app = Flask(__name__)
app.register_blueprint(user_point, url_prefix="/userPoint")


@app.route('/', methods=['GET'])
def main():
    return "Running..."


def init():
    init_db(app)


if __name__ == '__main__':
    init()
    app.run(host='0.0.0.0')
